PGraphics pg;

void setup() {
  size(640, 360);
  pg = createGraphics(640, 360);
  background(255);
  pg.stroke(0);
}

void draw() {
  stroke(0);
  if (mousePressed == true) {
    line(mouseX, mouseY, pmouseX, pmouseY);
    pg.beginDraw();
    pg.line(mouseX, mouseY, pmouseX, pmouseY);
    pg.endDraw();
  }
}

void keyPressed() {
  if(keyCode == 10) {
    pg.save("signature.png");
    exit();
  } else if(keyCode == 8) {
    pg.beginDraw();
    pg.clear();
    clear();
    background(255);
    pg.endDraw();
  }
}
