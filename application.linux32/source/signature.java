import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class signature extends PApplet {

PGraphics pg;

public void setup() {
  size(640, 360);
  pg = createGraphics(640, 360);
  background(255);
  pg.stroke(0);
}

public void draw() {
  stroke(0);
  if (mousePressed == true) {
    line(mouseX, mouseY, pmouseX, pmouseY);
    pg.beginDraw();
    pg.line(mouseX, mouseY, pmouseX, pmouseY);
    pg.endDraw();
  }
}

public void keyPressed() {
  if(keyCode == 10) {
    pg.save("signature.png");
    exit();
  } else if(keyCode == 8) {
    pg.beginDraw();
    pg.clear();
    clear();
    background(255);
    pg.endDraw();
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "signature" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
